function GenerarCa() {
    
    var Edades=generarAlt(18,99);
    var Altura=generarAlt(1.5,2.5);
    var PesoKg=generarAlt(20,130);

    document.getElementById("Edades").value=Edades.toFixed(0);
    document.getElementById("Altura").value=Altura.toFixed(2);
    document.getElementById("PesoKg").value=PesoKg.toFixed(1);
}
function Calcular() {
    let eda=document.getElementById("Edades").value
    if(eda==""){
        alert("Se debe de generar los valores requeridos")
    }
    else{
        var p=document.getElementById("PesoKg").value
        var a=document.getElementById("Altura").value
        var indecedemasa=(p/Math.pow(a,2));
        document.getElementById("indecedemasa").value=indecedemasa.toFixed(2)
        evaluar(indecedemasa.toFixed(2));
    }
    
}
function evaluar(indecedemasa) {
    if (indecedemasa<18.5) {
        document.getElementById("niv").value="Bajo Peso"
    }
    else if(indecedemasa>=18.5 && indecedemasa<=24.9){
        document.getElementById("niv").value="Peso saludable"
    }
    else if(indecedemasa>=25 && indecedemasa<=29.9){
        document.getElementById("niv").value="SobrePeso"
    }
    else if(indecedemasa>=30.0){
        document.getElementById("niv").value="Obesidad"
    }
}
var regis=0;
var acumulacion=0;
function Registrar() {
    
    let regi=document.getElementById('regi');
    let aux;
    let e=document.getElementById("Edades").value
    let p=document.getElementById("PesoKg").value
    let a=document.getElementById("Altura").value
    let indecedemasa=document.getElementById("indecedemasa").value
    let niv=document.getElementById("niv").value
    
    if(indecedemasa==""||e==""){
        alert("Se debe de generar los valores requeridos")
    }
    else{
        acumulacion=parseFloat(indecedemasa)+acumulacion;
        regis=regis+1;
        var prom=Number(acumulacion/regis);
        console.log(prom)
        
        aux="Ciudadano: "+regis+"  -- "+"Edad: "+e+"  -- "+"Altura: "+a+"  -- "+"Peso: "+p+"  -- "+"IMC: "+indecedemasa+"  -- "+"Nivel: "+niv+"  -- "+"<br>";
        document.getElementById("promedioimc").value=prom.toFixed(2);
        regi.innerHTML=regi.innerHTML+aux;
        limpiar();
    }
    
}


function generarAlt(min,max) {
    let p1 = max - min + 1;
    let p2 = Math.random() * p1;
    let r = Math.floor(p2) + min;
    return r;
}

function Borrar() {
    regis=0;
    document.getElementById("Edades").value   =""
    document.getElementById("PesoKg").value=""
    document.getElementById("Altura").value=""
    document.getElementById("indecedemasa").value=""
    document.getElementById("niv").value=""
    document.getElementById("promedioimc").value=""
    document.getElementById('regi').innerHTML="";
}
function limpiar(){
    document.getElementById("Edades").value   =""
    document.getElementById("PesoKg").value=""
    document.getElementById("Altura").value=""
    document.getElementById("indecedemasa").value=""
    document.getElementById("niv").value=""
}